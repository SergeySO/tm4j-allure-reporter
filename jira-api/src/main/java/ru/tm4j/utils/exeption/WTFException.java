package ru.tm4j.utils.exeption;

/**
 * <strong>W</strong>hat the <strong>T</strong>errible <strong>F</strong>ailure: потомок
 * {@code RuntimeException}, бросаемый в совершенно невероятных ситуациях, которые происходить
 * не должны были никогда. Пример: не поддерживается кодировка UTF-8, не установлен наш собственный
 * пакет и т.п. В приложении не должен вылететь вообще никогда.
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
public class WTFException extends RuntimeException {
    private static final String WHAT_THE_TERRIBLE_FAILURE = "What the Terrible Failure! ";

    /**
     * @see WTFException См. описание класса
     */
    public WTFException() {
        super(WHAT_THE_TERRIBLE_FAILURE);
    }

    /**
     * @see WTFException См. описание класса
     */
    public WTFException(String detailMessage, Throwable throwable) {
        super(WHAT_THE_TERRIBLE_FAILURE + detailMessage, throwable);
    }

    /**
     * @see WTFException См. описание класса
     */
    public WTFException(String detailMessage) {
        super(WHAT_THE_TERRIBLE_FAILURE + detailMessage);
    }

    /**
     * @see WTFException См. описание класса
     */
    public WTFException(Throwable throwable) {
        super(WHAT_THE_TERRIBLE_FAILURE, throwable);
    }
}
