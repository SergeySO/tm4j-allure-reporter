package ru.tm4j;

import lombok.AllArgsConstructor;
import lombok.Data;
import retrofit2.Response;
import ru.qa.model.JiraResult;

/**
 * @author Osinnikov S.S. on 16.11.2021
 */
@AllArgsConstructor
@Data
public class TestCaseResult {
    private Response response;
    private String id;
    private JiraResult jiraResult;

    public boolean isPosted() {
        return response.isSuccessful();
    }
}
