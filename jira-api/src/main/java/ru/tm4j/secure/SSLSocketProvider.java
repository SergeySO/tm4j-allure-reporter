package ru.tm4j.secure;

import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Провайдер для TLS сущностей.
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
public interface SSLSocketProvider {

    /**
     * @return Фабрика SSL-сокетов. В зависимости от реализации может поддерживать или не поддерживать TLS Certificate Pinning
     */
    SSLSocketFactory getSocketFactory();

    /**
     * @return Экземпляр X509TrustManager
     */
    X509TrustManager getX509TrustManager();
}
