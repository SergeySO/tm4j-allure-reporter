package ru.tm4j.secure;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Реализация {@link X509TrustManager}, которая не делает проверок цепочки сертификатов. Допустимо использовать его исключительно при работе
 * с тестовыми стендами, работающими на самоподписанных сертификатах
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
public class TrustAllManager implements X509TrustManager {
    @Override
    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

    }

    @Override
    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }
}
