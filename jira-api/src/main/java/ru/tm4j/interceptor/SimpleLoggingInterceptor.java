package ru.tm4j.interceptor;

import lombok.extern.slf4j.Slf4j;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

/**
 * @author Osinnikov S.S. on 24.11.2021
 */
@Slf4j
public class SimpleLoggingInterceptor implements Interceptor {

    @NotNull
    @Override public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        log.info("Requested headers:{}\nRequested URL:{}.", request.headers(), request.url());
        Response response = chain.proceed(request);
        log.info("Received headers: {}\nReceived URL:{}. Code: {}", response.headers(), response.request().url(), response.code());
        return response;
    }
}
