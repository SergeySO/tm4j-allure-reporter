package ru.tm4j.jira;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.*;
import ru.qa.model.credential.Credential;
import ru.qa.model.session.SessionData;
import ru.qa.model.testrun.TestRunRequest;
import ru.qa.model.testrun.TestRunResponse;
import ru.qa.model.testrun.testresult.TestResultRequest;
import ru.qa.model.testrun.testresult.TestResultResponse;

/**
 * @author Osinnikov S.S. on 25.11.2021
 */
public interface RepositoryInterface {
    @POST("auth/1/session")
    Call<SessionData> login(@Body Credential credential);

    @POST("atm/1.0/testrun")
    Call<TestRunResponse> createTestRun(@Body TestRunRequest testRunRequest);

    @POST("atm/1.0/testrun/{testRunKey}/testcase/{testKey}/testresult")
    Call<TestResultResponse> createTestResult(@Path("testRunKey") String testRunKey,
                                              @Path("testKey") String testKey,
                                              @Body TestResultRequest testResultRequest);
    @Multipart
    @POST("atm/1.0/testresult/{testResultId}/step/{stepIndex}/attachments")
    Call<Void> addAttachmentToTestCaseResult(@Path("testResultId") String testResultId,
                                             @Path("stepIndex") String stepIndex,
                                             @Part MultipartBody.Part filePart);
}
