package ru.tm4j.jira.models;

/**
 * @author Osinnikov S.S. on 24.11.2021
 */
public class MediaTypeData {
    public static final String IMAGE_PNG = "image/png";
}
