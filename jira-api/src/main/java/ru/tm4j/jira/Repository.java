package ru.tm4j.jira;

import ru.qa.model.JiraResult;
import ru.qa.model.session.SessionData;
import ru.qa.model.testrun.TestRunResponse;
import ru.tm4j.TestCaseResult;

import java.io.IOException;

/**
 * @author Osinnikov S.S. on 25.11.2021
 */
public interface Repository {
    SessionData connect(String login, String password) throws IOException;
    TestRunResponse createTestRun(String name, String projectKey) throws IOException;
    TestCaseResult createTestCaseResult(String testRunKey, JiraResult jiraResult) throws IOException;
    void addAttachmentToTestCaseResult(String pathAllureResult, TestCaseResult testCaseResult);
}
