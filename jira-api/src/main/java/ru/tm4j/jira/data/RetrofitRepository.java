package ru.tm4j.jira.data;

import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.qa.model.JiraResult;
import ru.qa.model.credential.Credential;
import ru.qa.model.session.SessionData;
import ru.qa.model.testrun.TestRunRequest;
import ru.qa.model.testrun.TestRunResponse;
import ru.qa.model.testrun.testresult.TestResultRequest;
import ru.qa.model.testrun.testresult.TestResultResponse;
import ru.tm4j.TestCaseResult;
import ru.tm4j.interceptor.SimpleLoggingInterceptor;
import ru.tm4j.configurations.APIConfiguration;
import ru.tm4j.jira.Repository;
import ru.tm4j.JavaNetCookieJar;
import ru.tm4j.jira.RepositoryInterface;
import ru.tm4j.utils.SSLUtils;

import javax.net.ssl.X509TrustManager;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Api Client для работы с Adaptavist
 *
 * @author Osinnikov S.S. on 25.11.2021
 */
@Slf4j
public class RetrofitRepository implements APIConfiguration, Repository {
    private final RepositoryInterface service;

    public RetrofitRepository(String apiBaseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiBaseUrl)
                .client(getOkHttpClientBuilderWithDefaultParams().build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(RepositoryInterface.class);
    }

    @Override
    public SessionData connect(@NotNull String login, @NotNull String password) throws IOException {
        Credential credential = new Credential(login, password);
        Call<SessionData> retrofitCall = service.login(credential);
        return (SessionData) checkResponseAndReturnObject(retrofitCall.execute());
    }

    @Override
    public TestRunResponse createTestRun(@NotNull String name, @NotNull String projectKey) throws IOException {
        return createTestRun(TestRunRequest.generateDefaultTestCycleRequest(name, projectKey));
    }

    public TestRunResponse createTestRun(@NotNull TestRunRequest testRunRequest) throws IOException {
        Call<TestRunResponse> retrofitCall = service.createTestRun(testRunRequest);
        return (TestRunResponse) checkResponseAndReturnObject(retrofitCall.execute());
    }

    private Object checkResponseAndReturnObject(Response response) throws IOException {
        if (!response.isSuccessful()) {
            throw new IOException(response.errorBody() != null
                    ? response.errorBody().string() : "Unknown error");
        }

        return response.body();
    }

    @Override
    public TestCaseResult createTestCaseResult(@NotNull String testRunKey, @NotNull JiraResult jiraResult) throws IOException {
        String testKey = jiraResult.getKey();
        TestResultRequest testResultRequest = jiraResult.getTestResultRequest();
        Call<TestResultResponse> retrofitCall = service.createTestResult(testRunKey, testKey, testResultRequest);
        Response<TestResultResponse> response = retrofitCall.execute();

        if (response.isSuccessful()) {
            TestResultResponse resultObject = response.body();
            return new TestCaseResult(response, resultObject.getId(), jiraResult);
        } else {
            return new TestCaseResult(response, null, jiraResult);
        }
    }

    @Override
    public void addAttachmentToTestCaseResult(@NotNull String pathAllureResult, @NotNull TestCaseResult testCaseResult) {
        if (testCaseResult.isPosted()) {
            testCaseResult.getJiraResult().getAttachmentsList().forEach(jiraAttachment ->
                    jiraAttachment.getAttachments().forEach(attachment -> {
                        try {
                            Thread.sleep(500);
                            File file = new File(String.format("%s/%s", pathAllureResult, attachment));
                            MultipartBody.Part filePart = MultipartBody.Part.
                                    createFormData("file",
                                            file.getName(),
                                            RequestBody.create(file, MediaType.parse("image/*")));
                            service.addAttachmentToTestCaseResult(
                                    testCaseResult.getId(), jiraAttachment.getIndex().toString(), filePart).execute();
                        } catch (InterruptedException | IOException e) {
                            e.printStackTrace();
                        }
                    }));
        }
    }

    private OkHttpClient.Builder getOkHttpClientBuilderWithDefaultParams() {
        return new OkHttpClient().newBuilder()
                .cookieJar(new JavaNetCookieJar())
                .addInterceptor(new SimpleLoggingInterceptor())
                .sslSocketFactory(SSLUtils.createSocketFactory(), (X509TrustManager) SSLUtils.getTrustManagers()[0])
                .hostnameVerifier((s, sslSession) -> true)
                .connectTimeout(100, TimeUnit.SECONDS)
                .writeTimeout(100, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);
    }
}
