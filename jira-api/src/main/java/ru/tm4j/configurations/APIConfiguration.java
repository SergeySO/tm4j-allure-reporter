package ru.tm4j.configurations;

/**
 * @author Osinnikov S.S. on 25.11.2021
 */
public interface APIConfiguration {
    static final String JSON_CONTENT_TYPE = "application/json";
}
