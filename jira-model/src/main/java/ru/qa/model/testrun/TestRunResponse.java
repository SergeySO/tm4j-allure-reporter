package ru.qa.model.testrun;

import lombok.Data;

/**
 * Класс для обработки данных внутри сессии jira (Ответ на запрос Test Run).
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
@Data
public class TestRunResponse {
    private Integer id;
    private String self;
    private String key;
}
