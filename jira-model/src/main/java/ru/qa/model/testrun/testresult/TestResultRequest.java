package ru.qa.model.testrun.testresult;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Класс для обработки данных внутри сессии jira (Для создания запроса Test Tesult).
 *
 * Example:
 * {
 *   "status": "Fail",
 *   "environment": "Firefox",
 *   "comment": "The test has failed on some automation tool procedure.",
 *   "assignedTo": "vitor.pelizza",
 *   "executedBy": "cristiano.caetano",
 *   "executionTime": 180000,
 *   "actualStartDate": "2016-02-14T19:22:00-0300",
 *   "actualEndDate": "2016-02-15T19:22:00-0300",
 *   "customFields": {
 *     "CI Server": "Bamboo"
 *   },
 *   "issueLinks": ["JQA-123", "JQA-456"],
 *   "scriptResults": [
 *     {
 *       "index": 0,
 *       "status": "Fail",
 *       "comment": "This step has failed."
 *     }
 *   ]
 * }
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
@Builder
public class TestResultRequest {
    private final String executedBy;
    private final long executionTime;
    private final String actualEndDate;
    private final String environment;
    private final List<String> issueLinks;
    private final String actualStartDate;
    private final List<ScriptResultItem> scriptResults;
    private final String comment;
    private final String assignedTo;
    private final String status;
}
