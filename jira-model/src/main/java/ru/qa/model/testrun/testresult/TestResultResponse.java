package ru.qa.model.testrun.testresult;

import lombok.Data;

/**
 * Класс для обработки данных внутри сессии jira (Ответ на запрос Test Result).
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
@Data
public class TestResultResponse {
    private String id;
}
