package ru.qa.model.testrun.testresult;

/**
 * Статусы тесткейсов в Jira
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
public class JiraCaseStatus {
    public static final String PASS = "Pass";
    public static final String FAILED = "Fail";
    public static final String NOT_EXECUTED = "Not Executed";
}
