package ru.qa.model.testrun.testresult;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Часть запроса запроса Test Result. (Результаты прохождения steps)
 *
 * Example:
 *
 * "scriptResults": [
 *   {
 *     "index": 0,
 *     "status": "Fail",
 *     "comment": "This step has failed."
 *   }
 * ]
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
@NoArgsConstructor
@Data
public class ScriptResultItem {
    private Integer index;
    private String status;
    private String comment;
}
