package ru.qa.model.testrun;

import lombok.Builder;
import java.util.List;

/**
 * Класс для обработки данных внутри сессии jira (Для создания запроса Test Run).
 *
 * Example:
 * 1. Simple request, using only required fields
 * {
 *   "name": "Full regression",
 *   "projectKey": "JQA"
 * }
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
@Builder
public class TestRunRequest {
    private final String name;
    private final String projectKey;
    private final String testPlanKey;
    private final String status;
    private final String iteration;
    private final String version;
    private final String folder;
    private final String issueKey;
    private final String owner;
    private final String plannedStartDate;
    private final String plannedEndDate;
    private final List<TestRunRequest> items;

    public static TestRunRequest generateDefaultTestCycleRequest(String name, String projectKey) {
        return testRunRequestBuilder(name, projectKey)
                .build();
    }

    public static TestRunRequest generateTestCycleRequest(String name, String projectKey, String folder) {
        return testRunRequestBuilder(name, projectKey)
                .folder(folder)
                .build();
    }

    public static TestRunRequest generateTestCycleRequest(String name, String projectKey, String folder, String version) {
        return testRunRequestBuilder(name, projectKey)
                .version(folder)
                .folder(version)
                .build();
    }

    private static TestRunRequest.TestRunRequestBuilder testRunRequestBuilder(String name, String projectKey) {
        return TestRunRequest.builder()
                .name(name)
                .projectKey(projectKey);
    }
}
