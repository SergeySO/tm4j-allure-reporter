package ru.qa.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.qa.model.attachment.JiraAttachment;
import ru.qa.model.testrun.testresult.TestResultRequest;

import java.util.List;

/**
 *  Класс обертка для хренения данных перед выгрузкой в jira.
 *
 * @author Osinnikov S.S. on 17.11.2021
 */
@NoArgsConstructor
@Data
public class JiraResult {
    private String key;
    private String projectKey;
    private TestResultRequest testResultRequest;
    private List<JiraAttachment> attachmentsList;
}
