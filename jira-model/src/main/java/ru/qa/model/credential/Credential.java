package ru.qa.model.credential;


/**
 * Класс данных для логина в jira
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
public class Credential {

    private String username;
    private String password;

    public Credential(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
