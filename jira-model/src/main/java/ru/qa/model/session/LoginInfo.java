package ru.qa.model.session;

import java.util.Date;

/**
 * Класс - хелпер для обработки сессии с Jira
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
public class LoginInfo {
    private int loginCount;
    private int failedLoginCount;
    private Date previousLoginTime;
    private Date lastFailedLoginTime;

    public int getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(int loginCount) {
        this.loginCount = loginCount;
    }

    public int getFailedLoginCount() {
        return failedLoginCount;
    }

    public void setFailedLoginCount(int failedLoginCount) {
        this.failedLoginCount = failedLoginCount;
    }

    public Date getPreviousLoginTime() {
        return previousLoginTime;
    }

    public void setPreviousLoginTime(Date previousLoginTime) {
        this.previousLoginTime = previousLoginTime;
    }

    public Date getLastFailedLoginTime() {
        return lastFailedLoginTime;
    }

    public void setLastFailedLoginTime(Date lastFailedLoginTime) {
        this.lastFailedLoginTime = lastFailedLoginTime;
    }

}
