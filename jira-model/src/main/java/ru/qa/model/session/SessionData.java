package ru.qa.model.session;

/**
 * Класс для обрабюотки данных внутри сессии с Jira
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
public class SessionData {
    private Session session;
    private LoginInfo loginInfo;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public LoginInfo getLoginInfo() {
        return loginInfo;
    }

    public void setLoginInfo(LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }
}
