package ru.qa.model.session;

/**
 * Класс - хелпер для обработки сессии с Jira
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
public class Session {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
