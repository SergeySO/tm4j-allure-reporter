package ru.qa.model.attachment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Osinnikov S.S. on 17.11.2021
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Pair<P, K> {
    private P projectKey;
    private K key;
}
