package ru.qa.model.attachment;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс хранения скриншотов для steps.
 *
 * @author Osinnikov S.S. on 17.11.2021
 */
@NoArgsConstructor
@Data
public class JiraAttachment {
    private Integer index;
    private List<String> attachments = new ArrayList<>();
}
