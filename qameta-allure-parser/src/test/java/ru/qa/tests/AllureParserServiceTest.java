package ru.qa.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.qa.model.JiraResult;
import ru.qa.service.AllureParserService;

import java.util.Collection;

/**
 * @author Osinnikov S.S. on 16.11.2021
 */
public class AllureParserServiceTest {
    @Test
    public void allureParserServiceTest() {
        AllureParserService allureModelConverter = new AllureParserService();
        Collection<JiraResult> testCaseResults = allureModelConverter
                .parseAndConvertToJiraResult("/Users/user/IdeaProjects/tm4j-allure-reporter/allure");
        Assertions.assertNotEquals(0, testCaseResults.size());
    }
}
