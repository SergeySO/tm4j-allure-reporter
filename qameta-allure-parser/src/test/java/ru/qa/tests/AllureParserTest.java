package ru.qa.tests;

import io.qameta.allure.model.TestResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.qa.parser.AllureParser;

import java.util.Collection;

/**
 * @author Osinnikov S.S. on 16.11.2021
 */
public class AllureParserTest {

    @Test
    public void allureParserTest() {
        AllureParser allureParser = new AllureParser("/Users/user/IdeaProjects/tm4j-allure-reporter/allure");
        Collection<TestResult> results = allureParser.parseToAllureModel();
        Assertions.assertNotEquals(0, results.size());
        System.out.println();
    }
}
