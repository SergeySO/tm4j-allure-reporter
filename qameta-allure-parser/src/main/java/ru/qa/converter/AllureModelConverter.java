package ru.qa.converter;

import io.qameta.allure.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import ru.qa.model.JiraResult;
import ru.qa.model.attachment.JiraAttachment;
import ru.qa.model.attachment.Pair;
import ru.qa.model.testrun.testresult.JiraCaseStatus;
import ru.qa.model.testrun.testresult.ScriptResultItem;
import ru.qa.model.testrun.testresult.TestResultRequest;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Класс конвертер из объекта allure в объект Jira
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
@Slf4j
public class AllureModelConverter {
    private static final String TMS_REGEX = "([A-Z]+){1,10}(-)(T\\d+)";
    private static final String TMS = "TMS";
    private static final String TEST_METHOD = "testMethod";
    private static final String HOST = "host";
    private static final Pattern TMS_PATTERN = Pattern.compile(TMS_REGEX);
    private final List<TestResult> testResults;

    public AllureModelConverter(List<TestResult> testResults) {
        if (CollectionUtils.isEmpty(testResults)) {
            log.error("I am error! Отсутствуют данные для конвертации");
            throw new RuntimeException("I am error! Отсутствуют данные для конвертации");
        }

        this.testResults = testResults;
    }

    public List<JiraResult> convertToJiraResult() {
        List<JiraResult> results = new ArrayList<>();
        for (TestResult testResult : testResults) {
            if (Objects.isNull(testResult)) {
                continue;
            }

            TestResultRequest testResultRequest = createTestResultRequest(testResult);

            JiraResult jiraResult = new JiraResult();
            Pair<String, String> projectKeyAndKey = convertKeyAndProjectKey(testResult.getLinks());

            if (projectKeyAndKey.getProjectKey() == null && projectKeyAndKey.getKey() == null) {
                continue;
            } else if (projectKeyAndKey.getProjectKey().isEmpty() && !projectKeyAndKey.getKey().isEmpty()) {
                continue;
            } else {
                jiraResult.setProjectKey(projectKeyAndKey.getProjectKey());
                jiraResult.setKey(projectKeyAndKey.getKey());
            }

            jiraResult.setTestResultRequest(testResultRequest);
            jiraResult.setAttachmentsList(convertAttachments(testResult));
            results.add(jiraResult);
        }

        return results;
    }

    @Deprecated
    public List<TestResultRequest> convert() {
        List<TestResultRequest> result = new ArrayList<>();

        for (TestResult testResult : testResults) {
            if (Objects.isNull(testResult)) {
                continue;
            }

            TestResultRequest testCaseResult = createTestResultRequest(testResult);
            result.add(testCaseResult);
        }

        return result;
    }

    private TestResultRequest createTestResultRequest(TestResult testResult) {
        TestResultRequest.TestResultRequestBuilder builder = TestResultRequest.builder();
        builder.status(convertStatus(testResult.getStatus()));
        builder.actualStartDate(convertMillisToStringDate(testResult.getStart()));
        builder.actualEndDate(convertMillisToStringDate(testResult.getStop()));
        builder.executionTime(executionTime(testResult.getStart(), testResult.getStop()));
        builder.comment(convertComment(testResult));
        builder.scriptResults(convertSteps(testResult));
        builder.issueLinks(convertLinks(testResult));
        return builder.build();
    }

    private String convertStatus(Status status) {
        switch (status) {
            case FAILED:
            case BROKEN:
                return JiraCaseStatus.FAILED;
            case SKIPPED:
                return JiraCaseStatus.NOT_EXECUTED;
            case PASSED:
                return JiraCaseStatus.PASS;
            default:
                return "";
        }
    }

    private List<JiraAttachment> convertAttachments(TestResult testResult) {
        List<JiraAttachment> attachments = new ArrayList<>();

        if (testResult.getSteps() != null) {
            List<StepResult> stepResults = testResult.getSteps();

            int index = 0;

            for (StepResult step : stepResults) {
                JiraAttachment attachment = new JiraAttachment();
                attachment.setIndex(index);

                for (Attachment at : step.getAttachments()) {
                    attachment.getAttachments().add(at.getSource());
                }

                if (step.getSteps() != null) {
                    for (StepResult t : step.getSteps()) {
                        for (Attachment at : t.getAttachments()) {
                            attachment.getAttachments().add(at.getSource());
                        }
                    }
                }

                attachments.add(attachment);
                index++;
            }
        }

        return attachments;
    }

    private Pair<String, String> convertKeyAndProjectKey(List<Link> links) {
        Pair<String, String> pair = new Pair<>();

        if (links != null && !links.isEmpty()) {
            List<Link> filtered = links.stream().filter(t -> t.getName().contains("-T")).collect(Collectors.toList());

            if (!filtered.isEmpty()) {
                String key = "";
                String projectKey = "";
                key = filtered.get(0).getName();
                projectKey = key.split("-T")[0];
                pair.setProjectKey(projectKey);
                pair.setKey(key);
                return pair;
            }
        }

        return pair;
    }

    private String convertMillisToStringDate(Long millis) {
        if (Objects.isNull(millis)) {
            return StringUtils.EMPTY;
        }

        return Instant.ofEpochMilli(millis)
                .atZone(ZoneId.systemDefault())
                .toOffsetDateTime()
                .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    private Long executionTime(Long start, Long stop) {
        if (start != null && stop != null) {
            return stop - start;
        }

        return 0L;
    }

    private String convertComment(TestResult testResult) {
        String comment = StringUtils.EMPTY;

        String testMethod = getLabelByName(TEST_METHOD, testResult.getLabels());

        String infoByErrorTestCase = getInfoByErrorTestCase(testResult);

        if (StringUtils.isNotBlank(testMethod)) {
            comment += testMethod;
        }

        if (StringUtils.isNotBlank(infoByErrorTestCase)) {
            comment += infoByErrorTestCase;
        }

        return comment.replace("\t"," ");
    }

    private String getLabelByName(String find, List<Label> labels) {
        if (CollectionUtils.isNotEmpty(labels)) {
            Optional<String> testMethod = labels
                    .stream()
                    .filter(Objects::nonNull)
                    .filter(it -> StringUtils.isNotBlank(it.getName()))
                    .filter(it -> it.getName().equalsIgnoreCase(find))
                    .map(Label::getValue)
                    .findFirst();
            return testMethod.orElseGet(() -> testMethod.orElse(StringUtils.EMPTY));
        }

        return StringUtils.EMPTY;
    }

    private String getInfoByErrorTestCase(TestResult testResult) {
        if (!convertStatus(testResult.getStatus()).equals(JiraCaseStatus.PASS) && Objects.nonNull(testResult.getStatusDetails())) {

            StringBuilder sb = new StringBuilder();

            if (Objects.nonNull(testResult.getFullName())) {
                sb.append("<b>").append(testResult.getFullName()).append("</b><br><br>");
            } else {
                sb.append("<b>").append(testResult.getName()).append("</b><br><br>");
            }

            String host = getLabelByName(HOST, testResult.getLabels());
            if (StringUtils.isNotBlank(host)) {
                sb.append("<b>host:</b> ").append(host).append("<br>");
            }

            sb.append("<b>known:</b> ").append(testResult.getStatusDetails().isKnown()).append("<br>");
            sb.append("<b>muted:</b> ").append(testResult.getStatusDetails().isMuted()).append("<br>");
            sb.append("<b>flacky:</b> ").append(testResult.getStatusDetails().isFlaky()).append("<br>");

            sb.append("<b>message:</b> ").append(StringUtils.replace(testResult.getStatusDetails().getMessage(), "\n", "<br>")).append("<br><br>");
            sb.append("<b>trace:</b> ").append(StringUtils.replace(testResult.getStatusDetails().getTrace(), "\n", "<br>")).append("<br><br>");

            return sb.toString();
        }

        return StringUtils.EMPTY;
    }

    private List<ScriptResultItem> convertSteps(TestResult testResult) {
        List<ScriptResultItem> scriptResultItems = new ArrayList<>();
        int index = 0;

        for (StepResult stepResult : testResult.getSteps()) {
            ScriptResultItem scriptResultItem = new ScriptResultItem();

            if (Objects.isNull(stepResult)) {
                continue;
            }

            scriptResultItem.setIndex(index);
            scriptResultItem.setStatus(convertStatus(stepResult.getStatus()));
            scriptResultItem.setComment(stepResult.getDescription());
            scriptResultItems.add(scriptResultItem);
            index++;
        }

        return scriptResultItems;
    }

    private List<String> convertLinks(TestResult testResult) {
        if (CollectionUtils.isNotEmpty(testResult.getLinks())) {
            return testResult.getLinks()
                    .stream()
                    .filter(Objects::nonNull)
                    .map(Link::getUrl)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }

        return null;
    }
}
