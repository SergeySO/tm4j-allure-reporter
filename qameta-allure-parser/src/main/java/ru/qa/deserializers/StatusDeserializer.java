package ru.qa.deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import io.qameta.allure.model.Status;

import java.lang.reflect.Type;

/**
 * @author Osinnikov S.S. on 25.11.2021
 */
public class StatusDeserializer implements JsonDeserializer<Status>
{
    @Override
    public Status deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException
    {
        Status[] statuses = Status.values();
        for (Status status : statuses)
        {
            if (status.value().equals(json.getAsString()))
                return status;
        }

        return null;
    }
}
