package ru.qa.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.qameta.allure.model.Status;
import ru.qa.deserializers.StatusDeserializer;

/**
 * @author Osinnikov S.S. on 25.11.2021
 */
public class J7Serializer {
    private static Gson gson = initGson();

    private static Gson initGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Status.class, new StatusDeserializer());
        return gsonBuilder.create();
    }

    public static Gson getGson() {
        return gson;
    }
}
