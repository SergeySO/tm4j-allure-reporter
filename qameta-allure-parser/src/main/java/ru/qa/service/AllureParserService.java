package ru.qa.service;

import io.qameta.allure.model.TestResult;
import ru.qa.converter.AllureModelConverter;
import ru.qa.model.JiraResult;
import ru.qa.model.testrun.testresult.TestResultRequest;
import ru.qa.parser.AllureParser;

import java.util.Collection;
import java.util.List;

/**
 * Сервис для парсинга и конверта тестового прогона
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
public class AllureParserService {
    @Deprecated
    public List<TestResultRequest> parseAndConvert(String path) {
        List<TestResult> testResults = new AllureParser(path).parseToAllureModel();
        return new AllureModelConverter(testResults).convert();
    }

    public List<JiraResult> parseAndConvertToJiraResult(String path) {
        List<TestResult> testResults = new AllureParser(path).parseToAllureModel();
        return new AllureModelConverter(testResults).convertToJiraResult();
    }
}
