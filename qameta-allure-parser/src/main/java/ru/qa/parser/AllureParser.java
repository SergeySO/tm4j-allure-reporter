package ru.qa.parser;

import io.qameta.allure.model.TestResult;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import ru.qa.helper.J7Serializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Парсер allure results (Allure 2!!!)
 *
 * @author Osinnikov S.S. on 16.11.2021
 */
@Slf4j
public class AllureParser {
    private static final String FILE_END_WITH = "-result.json";

    @Getter
    private final String path;

    public AllureParser(String path) {
        if (StringUtils.isBlank(path)) {
            log.error("Отсутствует путь до allure-results");
            throw new RuntimeException("I am error! Отсутствует путь до allure-results");
        }

        this.path = path;
    }

    public List<TestResult> parseToAllureModel() {
        File dir = new File(path);
        File[] files = dir.listFiles();

        if (files == null) {
            log.debug("Отсутствуют файлы allure-results.");
            return Collections.emptyList();
        }

        List<File> fileCollection = null;

        try {
            fileCollection = Files.walk(Paths.get(this.path))
                    .filter(Objects::nonNull)
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .filter(it -> it.getName().toLowerCase(Locale.ROOT).endsWith(FILE_END_WITH))
                    .collect(Collectors.toList());

            if (CollectionUtils.isEmpty(fileCollection)) {
                log.debug("Отсутствуют файлы allure-results.");
                return Collections.emptyList();
            }
        } catch (Exception e) {
            log.error("Произошла ошибка при чтении файлов.", e);
            return Collections.emptyList();
        }

        List<TestResult> results = new ArrayList<>(Collections.emptyList());
        for (File file : fileCollection) {
            TestResult testResult = null;

            try {
                testResult = J7Serializer.getGson().fromJson(new FileReader(file.getAbsolutePath()), TestResult.class);
            } catch (FileNotFoundException e) {
                log.error("Произошла ошибка при десериализации json.", e);
                return Collections.emptyList();
            }

            results.add(testResult);
        }

        return results;
    }
 }
